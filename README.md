# cashdenom
A Javascript Bookmarklet for finding the denominations of a given amount, to write on the back of a bank cheque.

The denominations are 100, 50, 10, 5, 1, 0.50, 0.25

It will calculate the minimum number of total notes required; i.e. try to use the biggest denominations whereever possible, starting from 100.


## Code

Copy this below code & save it as a bookmark.

````javascript
    javascript:(function()%7Bvar a%3Dprompt("Amount%5CnAMOUNT %5B100s%5D")%3Bif(""%3D%3D!a%26%26(a%3Da.split(" ")%2Camount%3DNumber(a%5B0%5D)%2C!isNaN(amount)))%7Bvar b%3D!1%3Bvoid 0!%3Da%5B1%5D%26%26(b%3D!0)%3Bvar c%3DArray(7)%3Bc%5B0%5D%3D100%3Bc%5B1%5D%3D50%3Bc%5B2%5D%3D10%3Bc%5B3%5D%3D5%3Bc%5B4%5D%3D1%3Bc%5B5%5D%3D.5%3Bc%5B6%5D%3D.25%3Bvar d%2Ce%3D%5B%5D%2Cf%3D%5B%5D%3Bfor(i%3D0%3Bi<c.length%3Bi%2B%2B)d%3DparseInt(amount%2Fc%5Bi%5D%2C10)%2Cb%26%26100%3D%3Dc%5Bi%5D%26%26a%5B1%5D<d%26%26(d%3Da%5B1%5D)%2Ce%5Bi%5D%3DNumber(d)%2Cf%5Bi%5D%3Dd*c%5Bi%5D%2Camount-%3Dd*c%5Bi%5D%3Bvar g%3D0%3Bfor(i%3D0%3Bi<e.length%3Bi%2B%2B)g%2B%3De%5Bi%5D%3Bvar h%3D""%3Bfor(i%3D0%3Bi<c.length%3Bi%2B%2B)h%2B%3Dc%5Bi%5D%2B" * "%2Be%5Bi%5D%2B" %3D "%2Bf%5Bi%5D%2B"%5Cn"%3Bh%2B%3D"Total %3D "%2Bg%3Balert(h)%7D%7D)()
````

## Usage:

  1. Click
  2. Input amount, space & then [optional] number of 100s needed (0 if none required).
  3. An alert box will show all the calculations & result.
  
## Examples:

    Input:
    12345.75

    Output:
    100 * 123 = 12300
    50 * 0 = 0
    10 * 4 = 40
    5 * 1 = 5
    1 * 0 = 0
    0.50 * 1 = 0.50
    0.25 * 1 = 0.25
    Total = 130

    -------------------

    Input:
    12345.75 10

    Output:
    100 * 10 = 1000
    50 * 226 = 11300
    10 * 4 = 40
    5 * 1 = 5
    1 * 0 = 0
    0.50 * 1 = 0.50
    0.25 * 1 = 0.25
    Total = 243
